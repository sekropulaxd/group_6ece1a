""""
#This program solves the roots of ax^2 + bx + c = 0 using quadratic equation
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""

import math

#user inputs required data
a = eval(input("the value of a of ax^2 + bx + c = 0: "))
b = eval(input("the value of b of ax^2 + bx + c = 0: "))
c = eval(input("the value of c of ax^2 + bx + c = 0: "))

#Computefor discriminant
disc = b ** 2 - (4 * a * c)

if disc > 0:
    r1 = (-b + math.sqrt(disc)) / (2 * a)
    r2 = (b + math.sqrt(disc)) / (2 * a)
    print("The root are: ", r1, r2)
elif disc == 0:
    r1 = (-b + math.sqrt(disc)) / (2 * a)
    r2 = (b + math.sqrt(disc)) / (2 * a)
    print("The root is: +-", r2)
else:
    print("the equation has no real roots,")