""""
This program enter the month and year and displays the number of days in the month.

Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""

#promt days_in_month():
year = eval(input("enter the year: "))
month = eval(input("enter the month (1-12): "))
# if 1 <= month <= 12: 
if month == 1:
    name_month = "January"
    day = 31
elif month == 2:
    name_month = "February"
    day = 28
    if year % 4 == 0:
        day = 29 
elif month == 3:
    name_month = "March"
    day = 31
elif month == 4:
    name_month = "April"
    day = 30
elif month == 5:
    name_month = "May"
    day = 31
elif month == 6:
    name_month = "June"
    day = 30
elif month == 7:
    name_month = "July"
    day = 31
elif month == 8:
    name_month = "August"
    day = 30
elif month == 9:
    name_month = "September"
    day = 31
elif month == 10:
    name_month = "October"
    day = 31
elif month == 11:
    name_month = "November"
    day = 30
elif month == 12:
    name_month = "December"
    day = 31
# Display results
print(f'the number of days in the month {name_month}: {day}')
# else:
#     print('invalid month entered. Please enter a number between 1 and 12.')