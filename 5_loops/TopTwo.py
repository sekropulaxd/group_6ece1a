"""
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""



def main():
    num_students = int(input("Enter the number of students: "))


    scores = []
    for i in range(1,1+num_students):
        score = float(input(f"Enter the score for student {i}: "))
        scores.append(score)


    scores.sort(reverse=True)


    if len(scores) >= 2:
        highest_score = scores[0]
        second_highest_score = scores[1]
        print(f"Highest score: {highest_score}")
        print(f"Second highest score: {second_highest_score}")
    elif len(scores) == 1:
        highest_score = scores[0]
        print(f"Highest score: {highest_score}")
        print("There is only one student, so there is no second highest score.")
    else:
        print("No score entered.")


if "_name_" =="_main_":
    main()