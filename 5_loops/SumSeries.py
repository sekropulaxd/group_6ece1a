"""
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""



def sum_series():
    total =0
    numerator = 1
    denominator = 3


    while numerator <= 97:
        total += numerator / denominator
        numerator += 2
        denominator += 2


    return total

result = sum_series()
print("sum of the series:", result)