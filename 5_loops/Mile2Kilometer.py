"""
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""



def display_conversion_table():
    print("Miles\tkilometers")
    print("--------------------")
    for mile in range(1, 11):
        kilometers = mile * 1.609
        print(f"{mile}\t{kilometers:.3f}")


display_conversion_table()