"""
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""

total = 0
count_positive = 0
count_negative = 0

#Read the first integer
num = int(input("Enter an integer (0 to end): "))

#Process input until 0 is entered
while num != 0:
    #check if the number is positive, negative, or zero
    if num > 0:
        count_positive += 1
    elif num < 0:
        count_negative += 1

    # Add the number to the total (excluding zeros)
    total += num

    # Read the next integer
    num = int(input("Enter an integer (0 to end): "))

# Calculate average (excluding zeros)
if count_positive + count_negative != 0:
    average = total / (count_positive + count_negative)
else: 
    average = 0

# Display results
print("\nResults:")
print("Total:", total)
print("Number of positive values:", count_positive)
print("Number of negative values:", count_negative)
print("Average (excluding zeros):", format(average, ' .2f'))
