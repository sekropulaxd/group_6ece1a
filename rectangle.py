"""
Celino, Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r
"""
class Rectangle:
    def __init__(self , width, height):
        self.width = width
        self.height = height

    def get_area(self):
       return self.width * self.height

    def get_perimeter(self):
       return 2 * (self.width + self.height)

    def is_square(self):
       return self.width == self.height

    def __str_(self):
       return f"Rectangle : with={self.width}, height={self.height}"

#for usage
rectangle1 = Rectangle(5, 3)
print(rectangle1)
print("Area: ", rectangle1.get_area())
print("Peremiter: ", rectangle1.get_peremiter())
print("is square: ", rectangle1.is_square())