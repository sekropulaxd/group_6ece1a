""
"""
This program program enter the length and draw of a star, and display the result.

Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B. 
"""
# turtle
import turtle 

def drew_star(length):

    turtle.penup()
    turtle.left(70)
    turtle.forward(length / 2)
    turtle.right(70)
    turtle.pendown()
    for _ in range (5):
       turtle.forward(length)
       turtle.right(144)
      
star_length = int(input("Enter the length of the star: "))
      
turtle.speed(4)
     
     
drew_star(star_length)
     
turtle.done()