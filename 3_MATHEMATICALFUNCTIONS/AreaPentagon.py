"""
This program prompts the user to enter the length from the center of a pentagon to
 a Vertex and computes the area ofthe Pentagon

Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B.

"""
import math
# Prompt the user to enter the length from the center to a vertex of the pentagon
radius = float(input("Enter the length from the center to a vertex of the pentagon: "))

# Compute the account value after sixth months
side_length = 2 * radius * math.sin(math.pi / 5)
    
area = ((3 * math.sqrt (3))/ 2) * side_length ** 2 


# Display results
print(f"The area of the pentagon is: {area}")
