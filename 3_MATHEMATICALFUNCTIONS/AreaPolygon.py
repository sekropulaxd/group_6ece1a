"""
This program that prompts the user to Enter the number of sides
 and their length of a regular polygon and displays it's area

Authors:
Authors:
Celino,Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r B.

"""
import math

# Prompt the user to enter the number of sides and side length
n = int(input("Enter the number of sides: "))
s = float(input("Enter the length of a side: "))

# Function to calculate the area of a regular polygon
def calculate_polygon_area(n, s):
    area = (n * s**2) / (4 * math.tan(math.pi / n))
    return area


# Calculate and print the area of the regular polygon
result = calculate_polygon_area(n, s)
print(f"The area of the regular polygon is {result:.2f}")