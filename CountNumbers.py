"""
Celino, Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r
"""

def count_occurrences(input_string):
    digit_counts = [0] * 10
    for char in input_string:
        if char.isdigit():
            digit_counts[int(char)] += 1
    
    max_count = max(digit_counts)
    most_common_digits = [str(i) for i in range(10) if digit_counts[i] == max_count]
    return ", ".join(most_common_digits)

user_input = input("Enter a string of numbers: ")
result = count_occurrences(user_input)
print(f"The number(s) that occur most often are: {result}")