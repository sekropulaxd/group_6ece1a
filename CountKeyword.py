"""
Celino, Red Ashlei L.
Bernardino, Jm S.
Aliet, Jay-r
"""

def count_occurrence_of_keywords(file_name):
    keywords = ["and", "as", "assert", "break", "class", "continue", "def", "del", "elif", "else", "except", "False", "finally", "for", "from", "global", "if", "import", "in", "is", "lambda", "None", "nonlocal", "not", "or", "pass", "raise", "return", "True", "try", "while", "with", "yield"]
    
    with open(file_name, 'r') as file:
        text = file.read()
    
    keyword_counts = {keyword: text.count(keyword) for keyword in keywords}
    
    return keyword_counts

# Prompt the user to enter the Python source code filename
file_name = input("Enter the Python source code filename: ")

# Call the function and print the results
keyword_counts = count_occurrence_of_keywords(file_name)

for keyword, count in keyword_counts.items():
    print(f"{keyword} occurs {count} times")